namespace MoviePet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Director = c.String(),
                        ReleaseYear = c.Int(nullable: false),
                        Rate = c.Int(nullable: false),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MovieCharacters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MovieId = c.Int(nullable: false),
                        Character = c.String(),
                        Actor = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.MovieId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MovieCharacters", new[] { "MovieId" });
            DropForeignKey("dbo.MovieCharacters", "MovieId", "dbo.Movies");
            DropTable("dbo.MovieCharacters");
            DropTable("dbo.Movies");
        }
    }
}
