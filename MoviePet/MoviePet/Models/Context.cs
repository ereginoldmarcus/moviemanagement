﻿using System.Data.Entity;

namespace MoviePet.Models
{
    public class Context : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> Characters { get; set; }
    }
}