﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoviePet.Models
{
    public class Movie
    {
        public Movie()
        {
            this.MovieCharacters = new HashSet<MovieCharacter>();
        }

        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public int ReleaseYear { get; set; }
        public int Rate { get; set; }
        public string Language { get; set; }

        public virtual ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}