﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoviePet.Models
{
    public class MovieCharacter
    {
        [Key]
        public int Id { get; set; }
        public int MovieId { get; set; }
        public string Character { get; set; }
        public string Actor { get; set; }
    }
}