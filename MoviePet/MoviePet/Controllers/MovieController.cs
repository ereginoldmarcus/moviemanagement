﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MoviePet.Models;

namespace MoviePet.Controllers
{
    public class MovieController : ApiController
    {
        private List<Item> directorList = new List<Item> { new Item { Id = 0, Name = "George Miller" }, new Item { Id = 1, Name = "Todd Haynes" }, new Item { Id = 2, Name = "J.J. Abrams" }, new Item { Id = 3, Name = "Tim Miller" }, new Item { Id = 4, Name = "Jon Favreau" }, new Item { Id = 4, Name = "Andrew Stanton" } };
        private List<Item> languageList = new List<Item> { new Item { Id = 0, Name = "English" }, new Item { Id = 1, Name = "Sinhala" }, new Item { Id = 2, Name = "Tamil" }, new Item { Id = 3, Name = "Hindi" } };


        [HttpPost]
        public HttpResponseMessage SaveMovie(Movie movie)
        {
            var db = new Context();

            if (movie.Id == 0 )
            {
                try
                {
                    db.Movies.Add(movie);
                    db.SaveChanges();
                }
                catch (Exception exception)
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, exception.Message);
                }
            }
            else
            {
                foreach (var character in movie.MovieCharacters)
                {
                    character.MovieId = movie.Id;
                    db.Characters.Add(character);
                }
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK, movie.Id);
        }

        [HttpGet]
        public IQueryable<Movie> GetAllMovies()
        {
            var db = new Context();
            return db.Movies;
        }

        [HttpGet]
        public HttpResponseMessage GetMovieDetail(string movieId)
        {
            var db = new Context();
            if (!string.IsNullOrEmpty(movieId))
            {
                var id = int.Parse(movieId);
                var movie = db.Movies.Single(e => e.Id == id);
                if (movie != null)
                {
                    if (!string.IsNullOrEmpty(movie.Director))
                    {
                        var directorId = int.Parse(movie.Director);
                        movie.Director = directorList.First(x => x.Id == directorId).Name;
                    }

                    if (!string.IsNullOrEmpty(movie.Language))
                    {
                        var languageId = int.Parse(movie.Language);
                        movie.Language = languageList.First(x => x.Id == languageId).Name;
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, movie);
                } 
            }
            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetDirectors()
        {
            return Request.CreateResponse(HttpStatusCode.OK, directorList);
        }

        [HttpGet]
        public HttpResponseMessage GetLanguages()
        {
            return Request.CreateResponse(HttpStatusCode.OK, languageList);
        }
    }
}
