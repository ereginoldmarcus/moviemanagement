/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="app/app.js" />
/// <reference path="app/listmovies/listmovieservice.js" />
/// <reference path="app/moviedetail/moviedetailcontroller.js" />
/// <reference path="app/moviedetail/moviedetailservice.js" />
/// <reference path="app/shared/directives/starrating.js" />
/// <reference path="app/listmovies/listmoviescontroller.js" />
/// <reference path="app/createmovie/createmovieservice.js" />
/// <reference path="app/createmovie/createmoviecontroller.js" />
