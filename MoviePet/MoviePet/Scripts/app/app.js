﻿var movie = angular.module('movieApplication', ['ngRoute']);
movie.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/addMovie', {
        templateUrl: '/Scripts/app/CreateMovie/CreateMovie.html',
        controller: 'createMovieController'
    }).when('/', {
        templateUrl: '/Scripts/app/ListMovies/ListMovies.html',
        controller: 'listMoviesController'
    }).when('/viewMovie/:movieId', {
        templateUrl: '/Scripts/app/MovieDetail/MovieDetail.html',
        controller: 'movieDetailController',
        resolve: {
            movie: function ($route,movieDetailService) {
                return movieDetailService.viewMovieData($route.current.params.movieId);
            }
        }
    });
}]);
