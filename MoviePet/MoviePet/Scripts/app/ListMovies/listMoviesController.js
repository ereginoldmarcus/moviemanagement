﻿var movie = angular.module("movieApplication");

movie.controller('listMoviesController', ['$scope', '$http', 'listMovieService', '$location', function ($scope, $http, listMovieService, $location) {
    
    listMovieService.getAllMovies().then(function (data) {
        $scope.movieList = data;
    });
    
    $scope.viewMovie = function (movieId) {
        $location.path('/viewMovie/' + movieId);
    };
    
    $scope.addMovie = function () {
        $location.path('/addMovie');
    };
}]);


