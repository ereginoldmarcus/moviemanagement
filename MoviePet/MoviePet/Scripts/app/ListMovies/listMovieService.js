﻿(function (angular) {
    var movie = angular.module("movieApplication");
    movie.service('listMovieService', ['$q', '$http', function ($q,$http) {

        this.getAllMovies = function () {
            var defer = $q.defer();

            $http.get('http://localhost:5913/api/Movie/GetAllMovies').success(function (data) {
                defer.resolve(data);
            }).error(function (data) {
                defer.reject(data);
            });
            return defer.promise;
        };
    }]);

})(angular);