﻿var movie = angular.module("movieApplication");

movie.controller("createMovieController", ['$scope', 'createMovieService', '$http', '$location', function ($scope, createMovieService, $http, $location) {
    $scope.MovieCharacters = [];
    $scope.AddMovie = function (newMovie) {
        newMovie.MovieCharacters = $scope.MovieCharacters;
        createMovieService.addmovie($http, $scope, newMovie);
    };

    $scope.AddCharacter = function () {
        var character = {
            Character: $scope.character,
            Actor: $scope.actor,
            MovieId: $scope.data.Id,
        };

        $scope.MovieCharacters.push(character);
        $scope.character = "";
        $scope.actor = "";
    };
    
    createMovieService.getDirectors($http, $scope);
    createMovieService.getLanguages($http, $scope);

    $scope.backToList = function() {
        $location.path('/');
    };
    //$scope.removeCharactor = function (index) {
    //    $scope.characters.splice(index, 1);
    //};
}]);