﻿(function(angular) {
    var movie = angular.module("movieApplication");
    movie.service('createMovieService', function () {
        var serviceResult = {};
        
        serviceResult.addmovie = function ($http, $scope, movieData) {
            $http.post('http://localhost:5913/api/Movie/SaveMovie', movieData).then(
                function (result) {
                    $scope.status = true;
                    $scope.data.Id = result.data;
                    alert('Saved Sucessfully!');
                },
                function (result) {
                    alert('Somthing went wrong!');
                });
        };
        
        serviceResult.getDirectors = function ($http, $scope) {
            $http.get('http://localhost:5913/api/Movie/GetDirectors').then(
                function (result) {
                    $scope.directors = result.data;
                },
                function (result) {
                    alert('Error loading Directors!');
                });
        };
        
        serviceResult.getLanguages = function ($http, $scope) {
            $http.get('http://localhost:5913/api/Movie/GetLanguages').then(
                function (result) {
                    $scope.languages = result.data;
                },
                function (result) {
                    alert('Error loading Languages!');
                });
        };
        return serviceResult;
    });
   
})(angular);