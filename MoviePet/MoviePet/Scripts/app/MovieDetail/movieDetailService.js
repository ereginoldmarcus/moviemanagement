﻿(function (angular) {
    var movie = angular.module("movieApplication");
    movie.service('movieDetailService', ['$q', '$http', function ($q, $http) {

        this.viewMovieData = function (movieId) {
            var deffered = $q.defer();
            $http.get('http://localhost:5913/api/Movie/GetMovieDetail', { params: { movieId: movieId } }).success(function (data) {
                deffered.resolve(data);
            }).error(function (data, status, headers, config) {
            });
            return deffered.promise;
        };
      
    }]);

})(angular);