﻿var movie = angular.module("movieApplication");

movie.controller('movieDetailController', ['$scope','$location', 'movie', function ($scope, $location, movie) {

    $scope.movieData = movie;
    
    $scope.backToList = function () {
        $location.path('/');
    };
}]);
